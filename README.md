This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Renders a simple tab component

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

I went with flexbox but the styling could have also been done a bit differently.
The structure is close but not exactly like what was provided.