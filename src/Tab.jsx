import React from "react";

export default class Tab extends React.Component {
  onClick = props => {
    console.log("onClick tab", this.props);
    props.onClick(props.tab);
  };

  render() {
    console.log("tab", this.props);
    const { tab, active } = this.props;
    const isActive = active ? 'tab selected' : 'tab';
    return (
      <div
        key={tab.id}
        onClick={() => this.props.onClick(tab)}
        className={isActive}
      >
        {tab.title}
      </div>
    );
  }
}
