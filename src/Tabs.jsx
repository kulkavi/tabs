import React, { Fragment } from "react";
import Tab from "./Tab";
import TabContent from "./TabContent";

export default class Tabs extends React.Component {
  tabs = [
    { id: "1", title: "Tab One", content: "This is the first tabs content" },
    { id: "2", title: "Tab Two", content: "This is the second tabs content" },
    { id: "3", title: "Tab Three", content: "This is the third tabs content" },
  ];
  state = {
    selectedTabId: null,
    selectedContent: null
  };

  onClick = selectedTab => {
    console.log("onClick tabs", this, selectedTab);
    this.setState({
      selectedTabId: selectedTab.id,
      selectedContent: selectedTab.content
    });
  };

  render() {
    return (
      <Fragment>
        <h1>Tabs</h1>
        <div className="tab-header">
          {this.tabs.map(tab => (
            <Tab
              active={this.state.selectedTabId === tab.id ? true : undefined}
              onClick={this.onClick}
              tab={tab}
            />
          ))}
        </div>
        <TabContent content={this.state.selectedContent} />
      </Fragment>
    );
  }
}
