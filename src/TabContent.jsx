import React from "react";

export default class TabContent extends React.Component {
  render() {
    console.log("tabcontent", this.props);

    return <div className="tab-content">{this.props.content}</div>;
  }
}
