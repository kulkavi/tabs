import React, { Component } from "react";
import logo from "./dharma_logo.svg";
import "./App.css";
import Tabs from "./Tabs";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <div>
          <Tabs />
        </div>
      </div>
    );
  }
}

export default App;
